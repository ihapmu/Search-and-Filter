import React, { Component } from 'react';
import './Filter.css'; 

class Filter extends Component {
    constructor() {
        super(); 
        this.state = {
            filter: {
                range: [0,0],
                textInput: '',
                singleSelection: '1',
                category:'option1', 
                multSelection:[],  
            }
        }

        //binding event handlers 
        this.handleCat = this.handleCat.bind(this); 
        this.handleMultSelect = this.handleMultSelect.bind(this); 
        this.handleSingleSelect = this.handleSingleSelect.bind(this); 
        this.handleFilter = this.handleFilter.bind(this); 
        this.handleRangeFrom = this.handleRangeFrom.bind(this); 
        this.handleRangeTo = this.handleRangeTo.bind(this); 
        this.handleTextInput = this.handleTextInput.bind(this); 
    }
    render() {
        return (
            <div className='container'>
                <div className='col-md-4'>
                    <div className='card'>
                        {/* Numeric Range Component */}
                        <div className='range'>
                            <span>
                                <label>Range</label>
                                <input className='range-from'
                                    name='from'
                                    placeholder='from'
                                    type='number'
                                    value={this.state.filter.range[0]}
                                    onChange={this.handleRangeFrom}
                                ></input>
                                <input className='range-to'
                                    name='to'
                                    placeholder='to'
                                    type='number'
                                    value={this.state.filter.range[1]}
                                    onChange={this.handleRangeTo}
                                ></input>
                            </span>
                        </div>

                        {/* Simple Text Input Component */}
                        <div className='text-input'>
                            <label>Text Input</label>
                            <input className='input-field'
                                type='text'
                                placeholder='Text Input'
                                onChange={this.handleTextInput}
                            ></input> 
                        </div>

                        {/* Single Selection Compoenent */}
                        <div className="single-select">
                            <label>Single Select</label>
                            <select className="form-control"
                                id="mult-selection" 
                                onChange={this.handleSingleSelect}>
                                <option value='1'>1</option>
                                <option value='2'>2</option>
                                <option value='3'>3</option>
                                <option value='4'>4</option>
                                <option value='5'>5</option>
                            </select>
                        </div>

                        {/* Category Selection Component */}
                        <label>Category Selection</label>
                        <div className="radio">
                        <label className="radio-label">
                            <input type="radio"
                                value="Category-A" 
                                name="customRadio"
                                checked={this.state.filter.category === "Category-A"}
                                onChange={this.handleCat}>
                            </input> 
                            Category A</label>
                        </div>
                        <div className="radio">
                        <label className="radio-label">
                            <input type="radio" 
                                value="Category-B" 
                                name="customRadio"
                                checked={this.state.filter.category === "Category-B"}
                                onChange={this.handleCat}>
                            </input> 
                            Category B</label>
                        </div>
                        <div className="radio">
                        <label className="radio-label">
                            <input type="radio" 
                                value="Category-C" 
                                name="customRadio"
                                checked={this.state.filter.category === "Category-C"}
                                onChange={this.handleCat}>
                            </input> 
                            Category C</label>
                        </div>

                        {/* Multiple Selection Compoenent */}
                        <label>Multiple Selection</label>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                            id="checkOne"
                            value="check-box1"
                            onChange={this.handleMultSelect}></input>
                            1</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox"
                                id="checkTwo"
                                value="check-box2"
                                onChange={this.handleMultSelect}></input>
                            2</label>
                        </div>
                        <div className="check-box">
                            <label>
                            <input type="checkbox" 
                                id="checkThree"
                                value="check-box3"
                                onChange={this.handleMultSelect}></input> 
                            3</label>
                        </div>
                        
                        {/* Filter Button Component */}
                        <button className='btn btn-primary' onClick={this.handleFilter}>Filter</button>
                    </div> 
                </div>
            </div> 
        ); 
    }

    //Handling category selection from Categories radio buttons. 
    handleCat(e) {
        let filter = Object.assign({}, this.state.filter); 
        filter.category = e.target.value; 
        this.setState({filter}); 
    }
    //Handling multiselection from multiple check boxs  
    handleMultSelect(e) {
        let optionOne = document.getElementById("checkOne").checked;
        let optionTwo = document.getElementById("checkTwo").checked;
        let optionThree = document.getElementById("checkThree").checked;
        let checkedOptions = [];  
        let filter = Object.assign({}, this.state.filter); 
        if (optionOne) {
            checkedOptions.push("Check-box1");
        }
        if (optionTwo) {
            checkedOptions.push("Check-box2");
        }
        if (optionThree) {
            checkedOptions.push("Check-box3");
        }
        filter.multSelection = checkedOptions; 
        this.setState({filter}); 
    }
    //Handling Numeric Range 'From' value.
    handleRangeFrom(e) {
        let filter = Object.assign({}, this.state.filter); 
        filter.range[0] = e.target.value; 
        this.setState({filter}); 
    }
    //Handling Numeric Range set 'From' 'To' another value. 
    handleRangeTo(e) {
        let filter = Object.assign({}, this.state.filter); 
        filter.range[1] = e.target.value; 
        this.setState({filter}); 
    }
    //Handling Text Input from the user.
    handleTextInput(e) { 
        let filter = Object.assign({}, this.state.filter); 
        filter.textInput = e.target.value; 
        this.setState({filter}); 
    }
    //Handling Single Selection from a dropdown.
    handleSingleSelect(e) {
        let filter = Object.assign({}, this.state.filter); 
        filter.singleSelection = e.target.value; 
        this.setState({filter}); 

    }
    //Logging the 'Filter Object' wich containes the parameters to be sent.
    //Should be modified next time to send the Query to the back-end. 
    handleFilter(e) {
        console.log(this.state.filter);
    }
}

export default Filter; 
